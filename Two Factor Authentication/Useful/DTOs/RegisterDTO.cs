﻿using System.ComponentModel.DataAnnotations;

namespace Two_Factor_Authentication.Useful.DTOs
{
    public class RegisterDTO
    {
        [Required]
        public string? email { get; set; }
    }
}
