﻿using System.ComponentModel.DataAnnotations;

namespace Two_Factor_Authentication.Useful.DTOs
{
    public class VerifyTokenDTO
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public DateTime tokenVerificationDate { get; set; }
    }
}
