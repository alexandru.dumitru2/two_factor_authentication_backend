﻿using System.ComponentModel.DataAnnotations;

namespace Two_Factor_Authentication.Useful.DTOs
{
    public class TokenDTO
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string email { get; set; }
        public DateTime tokenReceivingDate { get; set; }
    }
}
