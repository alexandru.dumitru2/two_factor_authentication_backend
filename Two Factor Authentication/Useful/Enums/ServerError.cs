﻿namespace Two_Factor_Authentication.Useful.Enums
{
    public class ServerError
    {
        public static readonly string USER_EXIST = "USER_EXIST";
        public static readonly string USER_NOT_FOUND = "USER_NOT_FOUND";
        public static readonly string TOKEN_DOES_NOT_MATCH = "TOKEN_DOES_NOT_MATCH";
        public static readonly string NO_DATE_PROVIDED = "NO_DATE_PROVIDED";
        public static readonly string TOKEN_EXPIRED = "TOKEN_EXPIRED";
        public static readonly string TOKEN_NOT_RECEIVED = "TOKEN_NOT_RECEIVED";
        public static readonly string DATE_INVALID = "DATE_INVALID";
    }
}
