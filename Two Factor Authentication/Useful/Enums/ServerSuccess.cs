﻿namespace Two_Factor_Authentication.Useful.Enums
{
    public class ServerSuccess
    {
        public static string USER_CREATED = "USER_CREATED";
        public static string TOKEN_RECEIVED_SUCCESSFULLY = "TOKEN_RECEIVED_SUCCESSFULLY";
        public static string TOKEN_VALID = "TOKEN_VALID";
    }
}
