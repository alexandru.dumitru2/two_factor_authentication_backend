﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Two_Factor_Authentication.Database.Entities
{
    public class User
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required, NotNull]
        [EmailAddress]
        public string? Email { get; set; }
        public string? OneTimePassword { get; set; }
        public DateTime PasswordReceivedDate { get; set; }
    }
}
