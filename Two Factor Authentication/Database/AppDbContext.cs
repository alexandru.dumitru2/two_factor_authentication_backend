﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using Two_Factor_Authentication.Database.Entities;

namespace Two_Factor_Authentication.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        //Don't think I am gonna need this, because I will probably have only one table,
        //but it is something I use to setup first, to avoid Delete Cascade on the database entities
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(x => x.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public DbSet<User>? Users { get; set; }
    }
}
