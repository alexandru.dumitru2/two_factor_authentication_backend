﻿using Microsoft.AspNetCore.Mvc;
using Two_Factor_Authentication.Database;
using Two_Factor_Authentication.Database.Entities;
using Two_Factor_Authentication.Useful.Enums;
using System.Security.Cryptography;
using Two_Factor_Authentication.Useful.DTOs;
using Two_Factor_Authentication.Useful.Services;

namespace Two_Factor_Authentication.Controllers
{
    [Route("api/authentication")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _context;
        private IConfiguration _config;
        private Sha256 _sha256;

        public UserController(AppDbContext context, IConfiguration config, Sha256 sha256)
        {
            _context = context;
            _config = config;
            _sha256 = sha256;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDTO register)
        {
            var userExist = _context.Users!.Any(x => x.Email == register.email);
            if (userExist)
            {
                return Conflict(new { status = false, errorMessage = ServerError.USER_EXIST });
            }

            var newUser = new User()
            {
                Email = register.email,
            };

            _context.Users!.Add(newUser);
            await _context.SaveChangesAsync();

            return Ok(new { status = true, message = ServerSuccess.USER_CREATED});
        }

        [HttpPost("new-token")]
        public async Task<IActionResult> NewToken([FromBody] RegisterDTO newToken)
        {
            var foundUser = _context.Users!.FirstOrDefault(x => x.Email == newToken.email);
            if(foundUser == null)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.USER_NOT_FOUND });
            }

            var computedStringToHash = newToken.email + _config["Token:Key"] + DateTime.Now;
            //Here we compute first 8 characters of the above computed string to use it as a one time password
            var oneTimePasswordHash = _sha256.ComputeSha256Hash(computedStringToHash).ToString().Substring(0,8).ToUpper();
            foundUser.OneTimePassword = oneTimePasswordHash;
            foundUser.PasswordReceivedDate = DateTime.MinValue;
            await _context.SaveChangesAsync();
            return Ok(new { status = true, token = oneTimePasswordHash });
        }

        [HttpPost("token-received")]
        public async Task<IActionResult> TokenRecieved([FromBody] TokenDTO tokenDTO)
        {
            var foundUser = _context.Users!.FirstOrDefault(x => x.Email == tokenDTO.email);
            if (foundUser == null)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.USER_NOT_FOUND });
            }

            if(foundUser.OneTimePassword != tokenDTO.Token)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.TOKEN_DOES_NOT_MATCH });
            }

            //verify if the date received in the body is less or equal than the actual time
            if(DateTime.Compare(tokenDTO.tokenReceivingDate, DateTime.UtcNow) <= 0)
            {
                foundUser.PasswordReceivedDate = tokenDTO.tokenReceivingDate;
            }
            else
            {
                return BadRequest(new { status = false, errorMessage = ServerError.DATE_INVALID, date = DateTime.UtcNow });
            }
            
            await _context.SaveChangesAsync();
            return Ok(new { status = true, message = ServerSuccess.TOKEN_RECEIVED_SUCCESSFULLY, date_received = foundUser.PasswordReceivedDate });
        }

        [HttpPost("verify-token")]
        public IActionResult VerifyToken([FromBody] VerifyTokenDTO tokenDTO)
        {
            var foundUser = _context.Users!.FirstOrDefault(x => x.Email == tokenDTO.email);

            //verify that user exist
            if (foundUser == null)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.USER_NOT_FOUND });
            }

            //verify that one time password of the user is the same as the one received in body
            if (foundUser.OneTimePassword != tokenDTO.Token)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.TOKEN_DOES_NOT_MATCH });
            }

            //verify if date recieved is less or equal than the current time
            if(DateTime.Compare(tokenDTO.tokenVerificationDate, DateTime.UtcNow) >= 0 || DateTime.Compare(tokenDTO.tokenVerificationDate.AddSeconds(2), DateTime.UtcNow) <= 0)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.DATE_INVALID });
            }

            //verify if the password was received before or not
            if (DateTime.Compare(foundUser.PasswordReceivedDate, DateTime.MinValue) == 0)
            {
                return BadRequest(new { status = false, errorMessage = ServerError.TOKEN_NOT_RECEIVED });
            }

            //verify if token is valid or not, by comparing the date time received in the body with the actual time the request is made
            if (DateTime.Compare(foundUser.PasswordReceivedDate.AddSeconds(30), tokenDTO.tokenVerificationDate) > 0)
            {
                return Ok(new { status = true, message = ServerSuccess.TOKEN_VALID });
            }
            else
            {
                return BadRequest(new { status = false, errorMessage = ServerError.TOKEN_EXPIRED });
            }
        }
    }
}
